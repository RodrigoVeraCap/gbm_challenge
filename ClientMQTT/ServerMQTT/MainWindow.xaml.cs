﻿using MQTTClient.Comunications;
using MQTTClient.Entities;
using MqttLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServerMQTT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string connectionString = "tcp://localhost:1883";
        private const string taxistaId = "6645bc1f-3b42-4f99-b280-a437046cc3f0";
        private string clienteId = string.Empty;
        private string viajeId = string.Empty;
        IMqtt client;
        private BackgroundWorker nuevoViaje;
        private BackgroundWorker envioUbicaciones;
        private BackgroundWorker envioViajeCreado;
        public MainWindow()
        {
            InitializeComponent();
            nuevoViaje = new System.ComponentModel.BackgroundWorker();
            envioUbicaciones = new BackgroundWorker();
            envioViajeCreado = new BackgroundWorker();
            envioUbicaciones.RunWorkerCompleted += EnvioUbicaciones_RunWorkerCompleted;
            nuevoViaje.RunWorkerCompleted += NuevoViaje_RunWorkerCompleted;
            envioViajeCreado.RunWorkerCompleted += EnvioViajeCreado_RunWorkerCompleted;
            OpenConnection();
           
        }

     

        private void OpenConnection()
        {
            client = MqttClientFactory.CreateClient(connectionString, taxistaId);
            client.Connect(true);
            client.Connected += new ConnectionDelegate(client_Connected);
            client.PublishArrived += new PublishArrivedDelegate(client_PublishArrived);
        }

      
        private bool client_PublishArrived(object sender, PublishArrivedArgs e)
        {
           RecivedMessage(e);
           return true;
        }

      
        private void client_Connected(object sender, EventArgs e)
        {
            RegisterSubscriptions();
        }
        private void NuevoViaje_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            PublishMessages(taxistaId, "mqttmessenger/blob/viajeIniciado");
        }

        private void buttonIniciaViaje_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(TextLatitudDestino.Text) && !string.IsNullOrEmpty(TextLatitudOrigen.Text) &&
                !string.IsNullOrEmpty(TextLongitudDestino.Text) && !string.IsNullOrEmpty(TextLongitudOrigen.Text))
            {
                nuevoViaje.RunWorkerAsync();
            }
        }
        private void RegisterSubscriptions()
        {
            client.Subscribe("mqttmessenger/blob/viajeIniciado", QoS.BestEfforts);
            client.Subscribe("mqttmessenger/blob/viajeIniciadoCliente", QoS.BestEfforts);
            client.Subscribe("mqttmessenger/blob/ubicacionTimpoReal", QoS.BestEfforts);
            client.Subscribe("mqttmessenger/blob/viajefinalizado", QoS.BestEfforts);
            client.Subscribe("mqttmessenger/blob/viajeCreado", QoS.BestEfforts);
        }

        private void PublishMessages(string message,string topic)
        {
            MqttPayload payload = new MqttPayload(message);
            client.Publish(topic, payload, QoS.BestEfforts, false);
        }

        private void RecivedMessage(PublishArrivedArgs e)
        {
            string messageAction = e.Topic.Split('/')[2];
            string topicPrefix = "mqttmessenger/";

            switch (messageAction)
            {
                case "viajeIniciadoCliente":
                    {
                        this.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            HttpClientMqt clientMqt = new HttpClientMqt();
                            var data = await clientMqt.ObtenerCliente(new Guid(e.Payload));
                            var cliente = JsonConvert.DeserializeObject<Cliente>(data);
                            Label_Cliente.Visibility = Visibility.Visible;
                            Label_ClienteNombre.Content = cliente.Nombre;
                            Label_ClienteApellidoP.Content = cliente.ApellidoPaterno;
                            Label_ClienteApellidoM.Content = cliente.ApellidoMaterno;
                            clienteId = cliente.IdCliente.ToString();
                            envioUbicaciones.RunWorkerAsync();
                        }));
                        
                        break;
                    }
            }
        }
        private void EnvioUbicaciones_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new Action(async () =>
            {
                var viaje = await CrearViajePostAsync();
                viajeId = viaje.IdViaje.ToString();
                envioViajeCreado.RunWorkerAsync();
            }));
            
        }

        private async Task<Viaje> CrearViajePostAsync()
        {
                     
                HttpClientMqt clientMqt = new HttpClientMqt();
                float latitudOrigen=0;
                float.TryParse(TextLatitudOrigen.Text,out latitudOrigen);
                float longitudOrigen=0;
                float.TryParse(TextLongitudOrigen.Text, out longitudOrigen);

                float latituddestino = 0;
                float.TryParse(TextLatitudDestino.Text, out latituddestino);
                float longituddestino = 0;
                float.TryParse(TextLongitudDestino.Text, out longituddestino);

                Viaje viaje = new Viaje()
                {
                    IdCliente = new Guid(clienteId),
                    IdTaxista = new Guid(taxistaId),
                    ViajeInicioLatitud = latitudOrigen,
                    ViajeInicioLongitud = longitudOrigen,
                    ViajeFinalLatitud = latituddestino,
                    ViajeFinalLongitud = longituddestino
                };
                var data = await clientMqt.CrearViajetAsync(viaje);
                var viajeData = JsonConvert.DeserializeObject<Viaje>(data);
                return viajeData;
        }

        private void EnvioViajeCreado_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EnviarViajeCreated();
            EnviarUbicacionesDummy();
        }
        private void EnviarViajeCreated()
        {
            PublishMessages("ViajeCreado", "mqttmessenger/blob/viajeCreado");
        }

        private async Task<Ubicacion> CrearUbicacionPostAsync(Ubicacion ubicacion)
        {

            HttpClientMqt clientMqt = new HttpClientMqt();
            var data = await clientMqt.CrearUbicacionAsync(ubicacion);
            var viajeData = JsonConvert.DeserializeObject<Viaje>(data);
            return ubicacion;
        }
        private void EnviarUbicacionesDummy()
        {
            this.Dispatcher.BeginInvoke(new Action(async () =>
            {
                foreach (var ubicacion in UbicacionesDummy())
                {
                    var viaje = await CrearUbicacionPostAsync(ubicacion);
                    TextCoordenadas.Text += $"{ubicacion.Latitud},{ubicacion.Longitud}\n";
                    PublishMessages(JsonConvert.SerializeObject(ubicacion), "mqttmessenger/blob/ubicacionTimpoReal");
                }
                PublishMessages("fin", "mqttmessenger/blob/viajefinalizado");
                client.Disconnect();
         }));
           
        }
        private List<Ubicacion> UbicacionesDummy()
        {
            List<Ubicacion> ubicaciones = new List<Ubicacion>();
            double lat = 0.234;
            double lon = 0.123;
            for(int i=0;i<30;i++)
            {
                Ubicacion ubicacion = new Ubicacion() { IdViaje = new Guid(viajeId), Latitud = 12.3243+lat, Longitud = -9.344+lon };
                ubicaciones.Add(ubicacion);
                lat += 0.123;
                lon += 0.345;
            }
            return ubicaciones;
        }
    }
}
