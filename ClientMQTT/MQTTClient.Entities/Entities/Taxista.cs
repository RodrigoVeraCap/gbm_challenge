﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MQTTClient.Entities
{
    public class Taxista
    {
        public Guid IdTaxista { get; set; }
        public string Nombre { get; set; }
        public string ApellidoMaterno { get; set; }
        public string ApellidoPaterno { get; set; }
        public string Placas { get; set; }
    }
}
