﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MQTTClient.Entities
{
    public class Ubicacion
    {
        public Guid IdUbicacion { get; set; }
        public Guid IdViaje { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
    }
}
