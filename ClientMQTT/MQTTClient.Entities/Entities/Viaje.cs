﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MQTTClient.Entities
{
    public class Viaje
    {
        public Guid IdViaje { get; set; }
        public Guid IdCliente { get; set; }
        public Guid IdTaxista { get; set; }
        public double ViajeInicioLatitud { get; set; }
        public double ViajeInicioLongitud { get; set; }
        public double ViajeFinalLatitud { get; set; }
        public double ViajeFinalLongitud { get; set; }
    }
}
