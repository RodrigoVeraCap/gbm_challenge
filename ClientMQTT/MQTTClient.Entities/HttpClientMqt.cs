﻿using MQTTClient.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Comunications
{
    public class HttpClientMqt
    {
        static string url = "http://localhost:31253";
        System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
        public HttpClientMqt()
        {
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<string> ObtenerTaxista(Guid idTaxista)
        {
            HttpResponseMessage responseMessage = await client.GetAsync($"api/taxista/{idTaxista}");
            if (responseMessage.IsSuccessStatusCode)
            {
                return await responseMessage.Content.ReadAsStringAsync();
            }
            return null;
        }

        public async Task<string> ObtenerCliente(Guid idCliente)
        {
            HttpResponseMessage responseMessage = await client.GetAsync($"/api/cliente/{idCliente}");
            if (responseMessage.IsSuccessStatusCode)
            {
                return await responseMessage.Content.ReadAsStringAsync();
            }
            return null;
        }
        public async Task<string> CrearViajetAsync(Viaje viaje)
        {
            var myContent = JsonConvert.SerializeObject(viaje);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await client.PostAsync("api/viajes", byteContent);
                                                                    response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            return null;
        }
        public async Task<string> CrearUbicacionAsync(Ubicacion ubicacion)
        {
            var myContent = JsonConvert.SerializeObject(ubicacion);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await client.PostAsync("api/ubicacion", byteContent);
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            return null;
        }
    }
}
