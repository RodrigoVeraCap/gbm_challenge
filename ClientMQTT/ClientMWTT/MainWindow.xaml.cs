﻿using MQTTClient.Comunications;
using MQTTClient.Entities;
using MqttLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace ClientMQTT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IMqtt client;
        private string path;
        private const string connectionString = "tcp://localhost:1883";
        private const string clientId = "c2112154-d356-4eea-adcc-a1f95cabe1bd";
        private BackgroundWorker nuevoViaje;
        public MainWindow()
        {
            InitializeComponent();
            nuevoViaje = new BackgroundWorker();
            nuevoViaje.RunWorkerCompleted += NuevoViaje_RunWorkerCompleted;
            Init();
        }

        

        public void Init()
        {
            client = MqttClientFactory.CreateClient(connectionString, clientId);
            client.Connect(true);
            client.Connected += new ConnectionDelegate(client_Connected);
            client.PublishArrived += new PublishArrivedDelegate(client_PublishArrived);
        }

       
        private bool client_PublishArrived(object sender, PublishArrivedArgs e)
        {
            RecivedMessage(e);
            return true;
        }

       
        private void client_Connected(object sender, EventArgs e)
        {
            client.Subscribe("mqttmessenger/blob/viajeIniciado", QoS.BestEfforts);
            client.Subscribe("mqttmessenger/blob/viajeIniciadoCliente", QoS.BestEfforts);
            client.Subscribe("mqttmessenger/blob/ubicacionTimpoReal", QoS.BestEfforts);
            client.Subscribe("mqttmessenger/blob/viajefinalizado", QoS.BestEfforts);
            client.Subscribe("mqttmessenger/blob/viajeCreado", QoS.BestEfforts);
        }

        private void RecivedMessage(PublishArrivedArgs e)
        {
            string messageAction = e.Topic.Split('/')[2];
            string topicPrefix = "mqttmessenger/";

            switch (messageAction)
            {
                case "viajeIniciado":
                    {
                        this.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            HttpClientMqt clientMqt = new HttpClientMqt();
                            var data = await clientMqt.ObtenerTaxista(new Guid(e.Payload));
                            
                            var taxita = JsonConvert.DeserializeObject<Taxista>(data);
                            Label_TaxistaNombre.Content = taxita.Nombre;
                            Label_TaxistaApellidoPaterno.Content = taxita.ApellidoPaterno;
                            Label_TaxistaApellidoMaterno.Content = taxita.ApellidoMaterno;
                            Label_TaxistaPlacas.Content = "PLACAS  " + taxita.Placas;
                            
                            Label_Taxista.Visibility = Visibility.Visible;


                        }));
                        nuevoViaje.RunWorkerAsync();
                        break;
                    }
                case "viajeCreado":
                    {
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {

                            Label_Viaje.Visibility = Visibility.Visible;
                        }));
                        break;
                    }
                case "ubicacionTimpoReal":
                    {
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            var ubicacion = JsonConvert.DeserializeObject<Ubicacion>(e.Payload);
                            TextCoordenadas.Text += $"{ubicacion.Latitud},{ubicacion.Longitud}\n";
                        }));
                        break;
                    }
                case "viajefinalizado":
                    {
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            Label_Viaje.Content = "VIAJE FINALIZADO";
                            client.Disconnect();
                        }));
                        break;
                    }
            }
        }

        private void NuevoViaje_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            PublishMessages(clientId, "mqttmessenger/blob/viajeIniciadoCliente");
        }
        private void PublishMessages(string message, string topic)
        {
            MqttPayload payload = new MqttPayload(message);
            client.Publish(topic, payload, QoS.BestEfforts, false);
        }
    }
}
