﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Helpers
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage
    }
}
