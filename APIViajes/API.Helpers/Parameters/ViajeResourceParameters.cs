﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Helpers
{
    public class ViajeResourceParameters :ResourceParameters
    {
        public Guid ClienteId { get; set; }
        public Guid TaxistaId { get; set; }
    }
}
