﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Helpers
{
    public class UbicacionResourceParameters : ResourceParameters
    {
        public Guid ViajeId { get; set; }
    }
}
