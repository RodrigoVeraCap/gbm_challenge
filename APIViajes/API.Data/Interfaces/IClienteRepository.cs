﻿using API.Data.Entities;
using API.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Data.Interfaces
{
    public interface IClienteRepository
    {
        PagedList<Cliente> GetClientes(ClienteResourceParameters parameters);
        Cliente GetCliente(Guid clienteId);
        void AddCliente(Cliente cliente);
        void DeleteCliente(Cliente cliente);
        void UpdateCliente(Cliente cliente);
        bool ClienteExists(Guid clienteId);
        bool Save();
    }
}
