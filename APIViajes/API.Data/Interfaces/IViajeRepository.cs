﻿using API.Data.Entities;
using API.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Data.Interfaces
{
    public interface IViajeRepository
    {
        PagedList<Viaje> GetViajes(ViajeResourceParameters parameters);
        Viaje GetViaje(Guid ViajeId);
        void AddViaje(Viaje Viaje);
        void DeleteViaje(Viaje Viaje);
        void UpdateViaje(Viaje Viaje);
        bool ViajeExists(Guid ViajeId);
        bool Save();
    }
}
