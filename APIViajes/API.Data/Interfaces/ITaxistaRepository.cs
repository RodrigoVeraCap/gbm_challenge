﻿using API.Data.Entities;
using API.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Data.Interfaces
{
    public interface ITaxistaRepository
    {
        PagedList<Taxista> GetTaxistas(TaxistaResourceParameters parameters);
        Taxista GetTaxista(Guid taxistaId);
        void AddTaxista(Taxista taxista);
        void DeleteTaxista(Taxista taxista);
        void UpdateTaxista(Taxista taxista);
        bool TaxistaExists(Guid taxistaId);
        bool Save();
    }
}
