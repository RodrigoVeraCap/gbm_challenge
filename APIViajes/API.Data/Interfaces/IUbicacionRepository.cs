﻿using API.Data.Entities;
using API.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Data.Interfaces
{
    public interface IUbicacionRepository
    {
        PagedList<Ubicacion> GetUbicacions(UbicacionResourceParameters parameters);
        Ubicacion GetUbicacion(Guid ubicacionId);
        void AddUbicacion(Ubicacion Ubicacion);
        void DeleteUbicacion(Ubicacion Ubicacion);
        void UpdateUbicacion(Ubicacion Ubicacion);
        bool UbicacionExists(Guid UbicacionId);
        bool Save();
    }
}
