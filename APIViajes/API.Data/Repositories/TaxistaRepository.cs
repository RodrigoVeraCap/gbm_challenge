﻿using API.Data.Entities;
using API.Data.Interfaces;
using API.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace API.Data.Repositories
{
    public class TaxistaRepository : ITaxistaRepository
    {
        private DemoTaxiContext _context;

        public TaxistaRepository(DemoTaxiContext context)
        {
            _context = context;
        }
        public void AddTaxista(Taxista taxista)
        {
            taxista.IdTaxista = Guid.NewGuid();
            _context.Taxista.Add(taxista);
        }

        public void DeleteTaxista(Taxista taxista)
        {
            _context.Taxista.Remove(taxista);
        }

        public Taxista GetTaxista(Guid taxistaId)
        {
            return _context.Taxista.FirstOrDefault(a => a.IdTaxista == taxistaId);
        }

        public PagedList<Taxista> GetTaxistas(TaxistaResourceParameters parameters)
        {
            var collectionBeforePaging =
               _context.Taxista
               .OrderBy(a => a.ApellidoPaterno)
               .ThenBy(a => a.ApellidoMaterno)
               .AsQueryable();

            if (!string.IsNullOrEmpty(parameters.Placas))
            {
                // trim & ignore casing
                var placasForWhereClause = parameters.Placas
                    .Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging
                    .Where(a => a.Placas.ToLowerInvariant() == placasForWhereClause);
            }

            if (!string.IsNullOrEmpty(parameters.SearchQuery))
            {
                // trim & ignore casing
                var searchQueryForWhereClause = parameters.SearchQuery
                    .Trim().ToLowerInvariant();

                collectionBeforePaging = collectionBeforePaging
                    .Where(a => a.Placas.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.ApellidoPaterno.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.ApellidoMaterno.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.Nombre.ToLowerInvariant().Contains(searchQueryForWhereClause));
            }

            return PagedList<Taxista>.Create(collectionBeforePaging,
                parameters.PageNumber,
                parameters.PageSize);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

        public bool TaxistaExists(Guid taxistaId)
        {
            return _context.Taxista.Any(a => a.IdTaxista == taxistaId);
        }

        public void UpdateTaxista(Taxista taxista)
        {
            _context.Taxista.Update(taxista);
        }
    }
}
