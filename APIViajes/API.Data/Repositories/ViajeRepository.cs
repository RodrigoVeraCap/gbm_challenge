﻿using API.Data.Entities;
using API.Data.Interfaces;
using API.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace API.Data.Repositories
{
    public class ViajeRepository : IViajeRepository
    {
        private DemoTaxiContext _context;

        public ViajeRepository(DemoTaxiContext context)
        {
            _context = context;
        }
        public void AddViaje(Viaje Viaje)
        {
            Viaje.IdViaje = Guid.NewGuid();
            _context.Viaje.Add(Viaje);
        }

        public void DeleteViaje(Viaje Viaje)
        {
            _context.Viaje.Remove(Viaje);
        }

        public Viaje GetViaje(Guid ViajeId)
        {
            return _context.Viaje.FirstOrDefault(a => a.IdViaje == ViajeId);
        }

        public PagedList<Viaje> GetViajes(ViajeResourceParameters parameters)
        {
            var collectionBeforePaging =
               _context.Viaje
               .OrderBy(a => a.IdTaxista)
               .ThenBy(a => a.IdViaje)
               .AsQueryable();

            if (parameters.ClienteId != Guid.Empty && parameters.TaxistaId!=Guid.Empty)
            {
                var clientForWhereClause = parameters.ClienteId;
                var taxiForWhereClause = parameters.TaxistaId;
                collectionBeforePaging = collectionBeforePaging
                 .Where(a => a.IdTaxista == taxiForWhereClause && a.IdCliente == clientForWhereClause);
            } else
            if (parameters.TaxistaId != Guid.Empty)
            {
                var taxiForWhereClause = parameters.TaxistaId;
                collectionBeforePaging = collectionBeforePaging
                 .Where(a => a.IdTaxista == taxiForWhereClause);
            }
            else
            if (parameters.ClienteId != Guid.Empty)
            {
                var clientForWhereClause = parameters.ClienteId;
                collectionBeforePaging = collectionBeforePaging
                 .Where(a => a.IdTaxista == clientForWhereClause);
            }
                                 
            return PagedList<Viaje>.Create(collectionBeforePaging,
                parameters.PageNumber,
                parameters.PageSize);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateViaje(Viaje Viaje)
        {
            _context.Viaje.Update(Viaje);
        }

        public bool ViajeExists(Guid ViajeId)
        {
            return _context.Viaje.Any(a => a.IdViaje == ViajeId);
        }
    }
}
