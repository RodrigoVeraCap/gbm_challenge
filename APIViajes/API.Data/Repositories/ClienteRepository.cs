﻿using API.Data.Entities;
using API.Data.Interfaces;
using API.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace API.Data.Repositories
{
    public class ClienteRepository : IClienteRepository
    {
        private DemoTaxiContext _context;

        public ClienteRepository(DemoTaxiContext context)
        {
            _context = context;
        }
        public void AddCliente(Cliente cliente)
        {
            cliente.IdCliente = Guid.NewGuid();
            _context.Cliente.Add(cliente);
        }

        public bool ClienteExists(Guid clienteId)
        {
            return _context.Cliente.Any(a => a.IdCliente == clienteId);
        }

        public void DeleteCliente(Cliente cliente)
        {
            _context.Cliente.Remove(cliente);
        }

        public Cliente GetCliente(Guid clienteId)
        {
            return _context.Cliente.FirstOrDefault(a => a.IdCliente == clienteId);
        }

        public PagedList<Cliente> GetClientes(ClienteResourceParameters parameters)
        {
            var collectionBeforePaging =
              _context.Cliente
              .OrderBy(a => a.ApellidoPaterno)
              .ThenBy(a => a.ApellidoMaterno)
              .AsQueryable();

           
            if (!string.IsNullOrEmpty(parameters.SearchQuery))
            {
                // trim & ignore casing
                var searchQueryForWhereClause = parameters.SearchQuery
                    .Trim().ToLowerInvariant();

                collectionBeforePaging = collectionBeforePaging
                    .Where(a => a.ApellidoPaterno.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.ApellidoMaterno.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.Nombre.ToLowerInvariant().Contains(searchQueryForWhereClause));
            }

            return PagedList<Cliente>.Create(collectionBeforePaging,
                parameters.PageNumber,
                parameters.PageSize);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateCliente(Cliente cliente)
        {
            _context.Cliente.Update(cliente);
        }
    }
}
