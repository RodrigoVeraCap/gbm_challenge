﻿using System;
using System.Collections.Generic;
using System.Text;
using API.Data.Entities;
using API.Data.Interfaces;
using API.Helpers;
using System.Linq;


namespace API.Data.Repositories
{
    public class UbicacionRepository : IUbicacionRepository
    {
        private DemoTaxiContext _context;

        public UbicacionRepository(DemoTaxiContext context)
        {
            _context = context;
        }
        public void AddUbicacion(Ubicacion ubicacion)
        {
            ubicacion.IdUbicacion = Guid.NewGuid();
            _context.Ubicacion.Add(ubicacion);
        }

        public void DeleteUbicacion(Ubicacion ubicacion)
        {
            _context.Ubicacion.Remove(ubicacion);
        }

        public Ubicacion GetUbicacion(Guid ubicacionId)
        {
            return _context.Ubicacion.FirstOrDefault(a => a.IdUbicacion == ubicacionId);
        }

        public PagedList<Ubicacion> GetUbicacions(UbicacionResourceParameters parameters)
        {
            var collectionBeforePaging =
              _context.Ubicacion
              .OrderBy(a => a.IdViaje)
             
              .AsQueryable();

            if (parameters.ViajeId != Guid.Empty)
            {
                var clientForWhereClause = parameters.ViajeId;
                collectionBeforePaging = collectionBeforePaging
                 .Where(a => a.IdViaje == clientForWhereClause);
            }

           return PagedList<Ubicacion>.Create(collectionBeforePaging,
                parameters.PageNumber,
                parameters.PageSize);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

        public bool UbicacionExists(Guid ubicacionId)
        {
            return _context.Ubicacion.Any(a => a.IdUbicacion == ubicacionId);
        }

        public void UpdateUbicacion(Ubicacion ubicacion)
        {
            _context.Ubicacion.Update(ubicacion);
        }
    }
}
