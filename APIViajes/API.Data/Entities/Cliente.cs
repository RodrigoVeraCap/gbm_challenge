﻿using System;
using System.Collections.Generic;

namespace API.Data.Entities
{
    public partial class Cliente
    {
        public Cliente()
        {
            Viaje = new HashSet<Viaje>();
        }

        public Guid IdCliente { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }

        public ICollection<Viaje> Viaje { get; set; }
    }
}
