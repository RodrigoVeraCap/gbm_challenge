﻿using System;
using System.Collections.Generic;

namespace API.Data.Entities
{
    public partial class Viaje
    {
        public Viaje()
        {
            Ubicacion = new HashSet<Ubicacion>();
        }

        public Guid IdViaje { get; set; }
        public Guid IdCliente { get; set; }
        public Guid IdTaxista { get; set; }
        public double ViajeInicioLatitud { get; set; }
        public double ViajeInicioLongitud { get; set; }
        public double ViajeFinalLatitud { get; set; }
        public double ViajeFinalLongitud { get; set; }

        public Cliente IdClienteNavigation { get; set; }
        public Taxista IdTaxistaNavigation { get; set; }
        public ICollection<Ubicacion> Ubicacion { get; set; }
    }
}
