﻿using System;
using System.Collections.Generic;

namespace API.Data.Entities
{
    public partial class Taxista
    {
        public Taxista()
        {
            Viaje = new HashSet<Viaje>();
        }

        public Guid IdTaxista { get; set; }
        public string Nombre { get; set; }
        public string ApellidoMaterno { get; set; }
        public string ApellidoPaterno { get; set; }
        public string Placas { get; set; }

        public ICollection<Viaje> Viaje { get; set; }
    }
}
