﻿using System;
using System.Collections.Generic;

namespace API.Data.Entities
{
    public partial class Ubicacion
    {
        public Guid IdUbicacion { get; set; }
        public Guid IdViaje { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }

        public Viaje IdViajeNavigation { get; set; }
    }
}
