﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace API.Data.Entities
{
    public partial class DemoTaxiContext : DbContext
    {
        public DemoTaxiContext()
        {
        }

        public DemoTaxiContext(DbContextOptions<DemoTaxiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Taxista> Taxista { get; set; }
        public virtual DbSet<Ubicacion> Ubicacion { get; set; }
        public virtual DbSet<Viaje> Viaje { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-0NK0SQT\\SQLEXPRESS;Database=DemoTaxi;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.IdCliente);

                entity.Property(e => e.IdCliente)
                    .HasColumnName("Id_Cliente")
                    .ValueGeneratedNever();

                entity.Property(e => e.ApellidoMaterno)
                    .HasColumnName("Apellido_Materno")
                    .HasMaxLength(50);

                entity.Property(e => e.ApellidoPaterno)
                    .HasColumnName("Apellido_Paterno")
                    .HasMaxLength(50);

                entity.Property(e => e.Nombre).HasMaxLength(50);
            });

            modelBuilder.Entity<Taxista>(entity =>
            {
                entity.HasKey(e => e.IdTaxista);

                entity.Property(e => e.IdTaxista)
                    .HasColumnName("Id_Taxista")
                    .ValueGeneratedNever();

                entity.Property(e => e.ApellidoMaterno)
                    .HasColumnName("Apellido_Materno")
                    .HasMaxLength(50);

                entity.Property(e => e.ApellidoPaterno)
                    .HasColumnName("Apellido_Paterno")
                    .HasMaxLength(50);

                entity.Property(e => e.Nombre).HasMaxLength(50);

                entity.Property(e => e.Placas).HasMaxLength(10);
            });

            modelBuilder.Entity<Ubicacion>(entity =>
            {
                entity.HasKey(e => e.IdUbicacion);

                entity.Property(e => e.IdUbicacion)
                    .HasColumnName("Id_Ubicacion")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdViaje).HasColumnName("Id_Viaje");

                entity.HasOne(d => d.IdViajeNavigation)
                    .WithMany(p => p.Ubicacion)
                    .HasForeignKey(d => d.IdViaje)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Ubicacion_Viaje");
            });

            modelBuilder.Entity<Viaje>(entity =>
            {
                entity.HasKey(e => e.IdViaje);

                entity.Property(e => e.IdViaje)
                    .HasColumnName("Id_Viaje")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdCliente).HasColumnName("Id_Cliente");

                entity.Property(e => e.IdTaxista).HasColumnName("Id_Taxista");

                entity.Property(e => e.ViajeFinalLatitud).HasColumnName("Viaje_Final_Latitud");

                entity.Property(e => e.ViajeFinalLongitud).HasColumnName("Viaje_Final_Longitud");

                entity.Property(e => e.ViajeInicioLatitud).HasColumnName("Viaje_Inicio_Latitud");

                entity.Property(e => e.ViajeInicioLongitud).HasColumnName("Viaje_Inicio_Longitud");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Viaje)
                    .HasForeignKey(d => d.IdCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Viaje_Cliente");

                entity.HasOne(d => d.IdTaxistaNavigation)
                    .WithMany(p => p.Viaje)
                    .HasForeignKey(d => d.IdTaxista)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Viaje_Taxista");
            });
        }
    }
}
