﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Interfaces;
using API.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Data.Entities;
using APIViajes.Models;

namespace APIUbicacions.Controllers
{
    [Route("api/ubicacion")]
    [ApiController]
    public class UbicacionController : ControllerBase { 
     IUbicacionRepository _UbicacionRepository;
        IViajeRepository _viajeRepository;
        private IUrlHelper _urlHelper;
        public UbicacionController(IUbicacionRepository UbicacionRepository, IViajeRepository viajeRepository, IUrlHelper urlHelper)
        {
            _UbicacionRepository = UbicacionRepository;
            _viajeRepository = viajeRepository;
            _urlHelper = urlHelper;
        }

    [HttpGet(Name = "GetUbicacions")]
    public IActionResult GetUbicacions(UbicacionResourceParameters UbicacionResourceParameters)
    {
        var UbicacionsFromRepo = _UbicacionRepository.GetUbicacions(UbicacionResourceParameters);
            if (Response != null)
            {
                var previousPageLink = UbicacionsFromRepo.HasPrevious ?
                    CreateUbicacionsResourceUri(UbicacionResourceParameters,
                    ResourceUriType.PreviousPage) : null;

                var nextPageLink = UbicacionsFromRepo.HasNext ?
                    CreateUbicacionsResourceUri(UbicacionResourceParameters,
                    ResourceUriType.NextPage) : null;

                var paginationMetadata = new
                {
                    totalCount = UbicacionsFromRepo.TotalCount,
                    pageSize = UbicacionsFromRepo.PageSize,
                    currentPage = UbicacionsFromRepo.CurrentPage,
                    totalPages = UbicacionsFromRepo.TotalPages,
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink
                };

                Response.Headers.Add("X-Pagination",
                    Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));
            }
        var ubicaciones = Mapper.Map<IEnumerable<UbicacionDto>>(UbicacionsFromRepo);
        return Ok(ubicaciones);
    }

    private string CreateUbicacionsResourceUri(
        UbicacionResourceParameters UbicacionResourceParameters,
        ResourceUriType type)
    {
        switch (type)
        {
            case ResourceUriType.PreviousPage:
                return _urlHelper.Link("GetUbicacions",
                  new
                  {
                     
                      ViajeId = UbicacionResourceParameters.ViajeId,
                      pageNumber = UbicacionResourceParameters.PageNumber - 1,
                      pageSize = UbicacionResourceParameters.PageSize
                  });
            case ResourceUriType.NextPage:
                return _urlHelper.Link("GetUbicacions",
                  new
                  {

                      ViajeId = UbicacionResourceParameters.ViajeId,
                      pageNumber = UbicacionResourceParameters.PageNumber + 1,
                      pageSize = UbicacionResourceParameters.PageSize
                  });

            default:
                return _urlHelper.Link("GetUbicacions",
                new
                {

                    ViajeId = UbicacionResourceParameters.ViajeId,
                    pageNumber = UbicacionResourceParameters.PageNumber,
                    pageSize = UbicacionResourceParameters.PageSize
                });
        }
    }

    [HttpGet("{id}", Name = "GetUbicacion")]
    public IActionResult GetUbicacion(Guid id)
    {
        var UbicacionsFromRepo = _UbicacionRepository.GetUbicacion(id);

        if (UbicacionsFromRepo == null)
        {
            return NotFound(id);
        }

        var Ubicacion = Mapper.Map<UbicacionDto>(UbicacionsFromRepo);
        return Ok(Ubicacion);
    }

    [HttpPost]
    public IActionResult CreateUbicacion([FromBody] UbicacionDto Ubicacion)
    {
        if (Ubicacion == null)
        {
            return BadRequest();
        }
            if (!_viajeRepository.ViajeExists(Ubicacion.IdViaje))
            {
                return NotFound(Ubicacion.IdViaje);
            }

            var UbicacionEntity = Mapper.Map<Ubicacion>(Ubicacion);

        _UbicacionRepository.AddUbicacion(UbicacionEntity);

        if (!_UbicacionRepository.Save())
        {
            return StatusCode(500, "Hay un problema al procesar su petición");
        }

        var UbicacionoReturn = Mapper.Map<UbicacionDto>(UbicacionEntity);

        return CreatedAtRoute("GetUbicacion",
            new { id = UbicacionoReturn.IdUbicacion },
            UbicacionoReturn);
    }

        [HttpPut("{id}")]
        public IActionResult UpdateUbicacion([FromBody] UbicacionDto Ubicacion, Guid id)
        {
            if (Ubicacion == null)
            {
                return BadRequest();
            }
            if (id == Guid.Empty)
            {
                return BadRequest();
            }
            var UbicacionFromRepo = _UbicacionRepository.GetUbicacion(id);
            if (UbicacionFromRepo == null)
            {
                return NotFound(id);
            }

            Mapper.Map(Ubicacion, UbicacionFromRepo);
            UbicacionFromRepo.IdUbicacion = id;
            _UbicacionRepository.UpdateUbicacion(UbicacionFromRepo);

            if (!_UbicacionRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            var UbicacionoReturn = Mapper.Map<UbicacionDto>(Ubicacion);

            return CreatedAtRoute("GetUbicacion",
                new { id = UbicacionoReturn.IdUbicacion },
                UbicacionoReturn);
        }

    [HttpDelete("{id}")]
    public IActionResult DeleteUbicacion(Guid id)
    {
        var UbicacionFromRepo = _UbicacionRepository.GetUbicacion(id);
        if (UbicacionFromRepo == null)
        {
            return NotFound(id);
        }

        _UbicacionRepository.DeleteUbicacion(UbicacionFromRepo);

        if (!_UbicacionRepository.Save())
        {
            return StatusCode(500, "Hay un problema al procesar su petición");
        }

        return NoContent();
    }

}
}
