﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Interfaces;
using API.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Data.Entities;
using APIViajes.Models;

namespace APIViajes.Controllers
{
    [Route("api/viajes")]
    [ApiController]
    public class ViajeController : ControllerBase
    {
        IViajeRepository _viajeRepository;
        IClienteRepository _clienteRepository;
        ITaxistaRepository _taxistaRepository;

        private IUrlHelper _urlHelper;
        public ViajeController(IViajeRepository viajeRepository,IClienteRepository clienteRepository, ITaxistaRepository taxistaRepository, IUrlHelper urlHelper)
        {
            _viajeRepository = viajeRepository;
            _clienteRepository = clienteRepository;
            _taxistaRepository = taxistaRepository;
            _urlHelper = urlHelper;
        }

        [HttpGet(Name = "GetViajes")]
        public IActionResult GetViajes(ViajeResourceParameters ViajeResourceParameters)
        {
            var ViajesFromRepo = _viajeRepository.GetViajes(ViajeResourceParameters);
            if (Response != null)
            {
                var previousPageLink = ViajesFromRepo.HasPrevious ?
                    CreateViajesResourceUri(ViajeResourceParameters,
                    ResourceUriType.PreviousPage) : null;

                var nextPageLink = ViajesFromRepo.HasNext ?
                    CreateViajesResourceUri(ViajeResourceParameters,
                    ResourceUriType.NextPage) : null;

                var paginationMetadata = new
                {
                    totalCount = ViajesFromRepo.TotalCount,
                    pageSize = ViajesFromRepo.PageSize,
                    currentPage = ViajesFromRepo.CurrentPage,
                    totalPages = ViajesFromRepo.TotalPages,
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink
                };

                Response.Headers.Add("X-Pagination",
                    Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));
            }
            var viajes = Mapper.Map<IEnumerable<ViajeDto>>(ViajesFromRepo);
            return Ok(viajes);
        }

        private string CreateViajesResourceUri(
            ViajeResourceParameters ViajeResourceParameters,
            ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetViajes",
                      new
                      {
                          TaxistaId = ViajeResourceParameters.TaxistaId,
                          ClienteId = ViajeResourceParameters.ClienteId,
                          pageNumber = ViajeResourceParameters.PageNumber - 1,
                          pageSize = ViajeResourceParameters.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetViajes",
                      new
                      {
                          TaxistaId = ViajeResourceParameters.TaxistaId,
                          ClienteId = ViajeResourceParameters.ClienteId,
                          pageNumber = ViajeResourceParameters.PageNumber + 1,
                          pageSize = ViajeResourceParameters.PageSize
                      });

                default:
                    return _urlHelper.Link("GetViajes",
                    new
                    {
                        TaxistaId = ViajeResourceParameters.TaxistaId,
                        ClienteId = ViajeResourceParameters.ClienteId,
                        pageNumber = ViajeResourceParameters.PageNumber,
                        pageSize = ViajeResourceParameters.PageSize
                    });
            }
        }

        [HttpGet("{id}", Name = "GetViaje")]
        public IActionResult GetViaje(Guid id)
        {
            var ViajesFromRepo = _viajeRepository.GetViaje(id);

            if (ViajesFromRepo == null)
            {
                return NotFound();
            }

            var Viaje = Mapper.Map<ViajeDto>(ViajesFromRepo);
            return Ok(Viaje);
        }

        [HttpPost]
        public IActionResult CreateViaje([FromBody] ViajeDto Viaje)
        {
            if (Viaje == null)
            {
                return BadRequest();
            }
            if(!_clienteRepository.ClienteExists(Viaje.IdCliente))
            {
                return NotFound(Viaje.IdCliente);
            }
            if(!_taxistaRepository.TaxistaExists(Viaje.IdTaxista))
            {
                return NotFound(Viaje.IdTaxista);
            }

            var ViajeEntity = Mapper.Map<Viaje>(Viaje);

            _viajeRepository.AddViaje(ViajeEntity);

            if (!_viajeRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            var ViajeoReturn = Mapper.Map<ViajeDto>(ViajeEntity);

            return CreatedAtRoute("GetViaje",
                new { id = ViajeoReturn.IdViaje },
                ViajeoReturn);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateViaje([FromBody] ViajeDto Viaje, Guid id)
        {
            if (Viaje == null)
            {
                return BadRequest();
            }
            if (id == Guid.Empty)
            {
                return BadRequest();
            }
            var ViajeFromRepo = _viajeRepository.GetViaje(id);
            if (ViajeFromRepo == null)
            {
                return NotFound(id);
            }

            Mapper.Map(Viaje, ViajeFromRepo);
            ViajeFromRepo.IdViaje = id;
            _viajeRepository.UpdateViaje(ViajeFromRepo);

            if (!_viajeRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            var ViajeoReturn = Mapper.Map<ViajeDto>(Viaje);

            return CreatedAtRoute("GetViaje",
                new { id = ViajeoReturn.IdViaje },
                ViajeoReturn);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteViaje(Guid id)
        {
            var ViajeFromRepo = _viajeRepository.GetViaje(id);
            if (ViajeFromRepo == null)
            {
                return NotFound(id);
            }

            _viajeRepository.DeleteViaje(ViajeFromRepo);

            if (!_viajeRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            return NoContent();
        }

    }
}
