﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Entities;
using API.Data.Interfaces;
using API.Helpers;
using APIViajes.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace APIViajes.Controllers
{
    [Route("api/taxista")]
    [ApiController]
    public class TaxistaController : ControllerBase
    {
        private ITaxistaRepository _taxistaRepository;
        private IUrlHelper _urlHelper;
        public TaxistaController(ITaxistaRepository taxistaRepository, IUrlHelper urlHelper)
        {
            _taxistaRepository = taxistaRepository;
            _urlHelper = urlHelper;
        }

        [HttpGet(Name = "GetTaxistas")]
        public IActionResult GetTaxistas([FromQuery]TaxistaResourceParameters taxistaResourceParameters)
        {
            var taxistasFromRepo = _taxistaRepository.GetTaxistas(taxistaResourceParameters);
            if (Response != null)
            {
                var previousPageLink = taxistasFromRepo.HasPrevious ?
                CreateTaxistasResourceUri(taxistaResourceParameters,
                ResourceUriType.PreviousPage) : null;

                var nextPageLink = taxistasFromRepo.HasNext ?
                    CreateTaxistasResourceUri(taxistaResourceParameters,
                    ResourceUriType.NextPage) : null;

                var paginationMetadata = new
                {
                    totalCount = taxistasFromRepo.TotalCount,
                    pageSize = taxistasFromRepo.PageSize,
                    currentPage = taxistasFromRepo.CurrentPage,
                    totalPages = taxistasFromRepo.TotalPages,
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink
                };

                Response.Headers.Add("X-Pagination",
                    Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));
            }
            var taxistas = Mapper.Map<IEnumerable<TaxistaDto>>(taxistasFromRepo);
            return Ok(taxistas);
        }

        private string CreateTaxistasResourceUri(
            TaxistaResourceParameters taxistaResourceParameters,
            ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetTaxistas",
                      new
                      {
                          searchQuery = taxistaResourceParameters.SearchQuery,
                          placas = taxistaResourceParameters.Placas,
                          pageNumber = taxistaResourceParameters.PageNumber - 1,
                          pageSize = taxistaResourceParameters.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetTaxistas",
                      new
                      {
                          searchQuery = taxistaResourceParameters.SearchQuery,
                          placas = taxistaResourceParameters.Placas,
                          pageNumber = taxistaResourceParameters.PageNumber + 1,
                          pageSize = taxistaResourceParameters.PageSize
                      });

                default:
                    return _urlHelper.Link("GetTaxistas",
                    new
                    {
                        searchQuery = taxistaResourceParameters.SearchQuery,
                        placas = taxistaResourceParameters.Placas,
                        pageNumber = taxistaResourceParameters.PageNumber,
                        pageSize = taxistaResourceParameters.PageSize
                    });
            }
        }

        [HttpGet("{id}", Name = "GetTaxista")]
        public IActionResult GetTaxista(Guid id)
        {
            var taxistasFromRepo = _taxistaRepository.GetTaxista(id);

            if (taxistasFromRepo == null)
            {
                return NotFound(id);
            }

            var taxista = Mapper.Map<TaxistaDto>(taxistasFromRepo);
            return Ok(taxista);
        }

        [HttpPost]
        public IActionResult CreateTaxista([FromBody] TaxistaDto taxista)
        {
            if (taxista == null)
            {
                return BadRequest();
            }

            var taxistaEntity = Mapper.Map<Taxista>(taxista);

            _taxistaRepository.AddTaxista(taxistaEntity);

            if (!_taxistaRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            var taxistaoReturn = Mapper.Map<TaxistaDto>(taxistaEntity);

            return CreatedAtRoute("GetTaxista",
                new { id = taxistaoReturn.IdTaxista },
                taxistaoReturn);
        }

        [HttpPut("{id}") ]
        public IActionResult UpdateTaxista([FromBody] TaxistaDto taxista,Guid id)
        {
            if (taxista == null)
            {
                return BadRequest();
            }
            if (id == Guid.Empty)
            {
                return BadRequest();
            }
            var taxistaFromRepo = _taxistaRepository.GetTaxista(id);
            if (taxistaFromRepo == null)
            {
                return NotFound(id);
            }

            Mapper.Map(taxista, taxistaFromRepo);
            taxistaFromRepo.IdTaxista = id;
            _taxistaRepository.UpdateTaxista(taxistaFromRepo);

            if (!_taxistaRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            var taxistaoReturn = Mapper.Map<TaxistaDto>(taxista);

            return CreatedAtRoute("GetTaxista",
                new { id = taxistaoReturn.IdTaxista },
                taxistaoReturn);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTaxista(Guid id)
        {
            var taxistaFromRepo = _taxistaRepository.GetTaxista(id);
            if (taxistaFromRepo == null)
            {
                return NotFound(id);
            }

            _taxistaRepository.DeleteTaxista(taxistaFromRepo);

            if (!_taxistaRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            return NoContent();
        }
    }
}