﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Interfaces;
using API.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Data.Entities;
using APIViajes.Models;

namespace APIViajes.Controllers
{
    [Route("api/Cliente")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private IClienteRepository _ClienteRepository;
        private IUrlHelper _urlHelper;
        public ClienteController(IClienteRepository ClienteRepository, IUrlHelper urlHelper)
        {
            _ClienteRepository = ClienteRepository;
            _urlHelper = urlHelper;
        }

        [HttpGet(Name = "GetClientes")]
        public IActionResult GetClientes([FromQuery]ClienteResourceParameters ClienteResourceParameters)
        {
    
                var ClientesFromRepo = _ClienteRepository.GetClientes(ClienteResourceParameters);
            if (Response != null)
            {
                var previousPageLink = ClientesFromRepo.HasPrevious ?
                    CreateClientesResourceUri(ClienteResourceParameters,
                    ResourceUriType.PreviousPage) : null;

                var nextPageLink = ClientesFromRepo.HasNext ?
                    CreateClientesResourceUri(ClienteResourceParameters,
                    ResourceUriType.NextPage) : null;

                var paginationMetadata = new
                {
                    totalCount = ClientesFromRepo.TotalCount,
                    pageSize = ClientesFromRepo.PageSize,
                    currentPage = ClientesFromRepo.CurrentPage,
                    totalPages = ClientesFromRepo.TotalPages,
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink
                };

                Response.Headers.Add("X-Pagination",
                    Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));
            }
            var clientes = Mapper.Map<IEnumerable<ClienteDto>>(ClientesFromRepo);
            return Ok(clientes);
        }

        private string CreateClientesResourceUri(
            ClienteResourceParameters ClienteResourceParameters,
            ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetClientes",
                      new
                      {
                          searchQuery = ClienteResourceParameters.SearchQuery,
                          pageNumber = ClienteResourceParameters.PageNumber - 1,
                          pageSize = ClienteResourceParameters.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetClientes",
                      new
                      {
                          searchQuery = ClienteResourceParameters.SearchQuery,
                          pageNumber = ClienteResourceParameters.PageNumber + 1,
                          pageSize = ClienteResourceParameters.PageSize
                      });

                default:
                    return _urlHelper.Link("GetClientes",
                    new
                    {
                        searchQuery = ClienteResourceParameters.SearchQuery,
                        pageNumber = ClienteResourceParameters.PageNumber,
                        pageSize = ClienteResourceParameters.PageSize
                    });
            }
        }

        [HttpGet("{id}", Name = "GetCliente")]
        public IActionResult GetCliente(Guid id)
        {
            var ClientesFromRepo = _ClienteRepository.GetCliente(id);

            if (ClientesFromRepo == null)
            {
                return NotFound(id);
            }

            var Cliente = Mapper.Map<ClienteDto>(ClientesFromRepo);
            return Ok(Cliente);
        }

        [HttpPost]
        public IActionResult CreateCliente([FromBody] ClienteDto Cliente)
        {
            if (Cliente == null)
            {
                return BadRequest();
            }

            var ClienteEntity = Mapper.Map<Cliente>(Cliente);

            _ClienteRepository.AddCliente(ClienteEntity);

            if (!_ClienteRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            var ClienteoReturn = Mapper.Map<ClienteDto>(ClienteEntity);

            return CreatedAtRoute("GetCliente",
                new { id = ClienteoReturn.IdCliente },
                ClienteoReturn);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCliente([FromBody] ClienteDto Cliente, Guid id)
        {
            if (Cliente == null)
            {
                return BadRequest();
            }
            if (id == Guid.Empty)
            {
                return BadRequest();
            }
            var clienteFromRepo = _ClienteRepository.GetCliente(id);
            if (clienteFromRepo == null)
            {
                return NotFound(id);
            }

            Mapper.Map(Cliente, clienteFromRepo);
            clienteFromRepo.IdCliente = id;
            _ClienteRepository.UpdateCliente(clienteFromRepo);

            if (!_ClienteRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            var clienteoReturn = Mapper.Map<ClienteDto>(Cliente);

            return CreatedAtRoute("Getcliente",
                new { id = clienteoReturn.IdCliente },
                clienteoReturn);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteCliente(Guid id)
        {
            var ClienteFromRepo = _ClienteRepository.GetCliente(id);
            if (ClienteFromRepo == null)
            {
                return NotFound(id);
            }

            _ClienteRepository.DeleteCliente(ClienteFromRepo);

            if (!_ClienteRepository.Save())
            {
                return StatusCode(500, "Hay un problema al procesar su petición");
            }

            return NoContent();
        }
    }
}