﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIViajes.Models
{
    public class TaxistaDto
    {
        public Guid IdTaxista { get; set; }
        public string Nombre { get; set; }
        public string ApellidoMaterno { get; set; }
        public string ApellidoPaterno { get; set; }
        public string Placas { get; set; }
    }
}
