﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIViajes.Models
{
    public class ViajeDto
    {
        public Guid IdViaje { get; set; }
        public Guid IdCliente { get; set; }
        public Guid IdTaxista { get; set; }
        public double ViajeInicioLatitud { get; set; }
        public double ViajeInicioLongitud { get; set; }
        public double ViajeFinalLatitud { get; set; }
        public double ViajeFinalLongitud { get; set; }
    }
}
