﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIViajes.Models
{
    public class UbicacionDto
    {
        public Guid IdUbicacion { get; set; }
        public Guid IdViaje { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
    }
}
