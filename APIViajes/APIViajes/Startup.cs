﻿using API.Data.Entities;
using API.Data.Interfaces;
using API.Data.Repositories;
using APIViajes.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Newtonsoft.Json.Serialization;

namespace APIViajes
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
                setupAction.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                setupAction.InputFormatters.Add(new XmlDataContractSerializerInputFormatter());
            })
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver =
                new CamelCasePropertyNamesContractResolver();
            });
            var connectionString = Configuration["connectionStrings:apiDBConnectionString"];
            services.AddDbContext<DemoTaxiContext>(o => o.UseSqlServer(connectionString));

            // register the repository
            services.AddScoped <IViajeRepository, ViajeRepository>();
            services.AddScoped<ITaxistaRepository, TaxistaRepository>();
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<IUbicacionRepository, UbicacionRepository>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped<IUrlHelper>(implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>()
                .ActionContext;
                return new UrlHelper(actionContext);
            });
           // services.AddScoped<IUrlHelper, UrlHelper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory, DemoTaxiContext demoTaxiContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandlerFeature != null)
                        {
                            var logger = loggerFactory.CreateLogger("Global exception logger");
                            logger.LogError(500,
                                exceptionHandlerFeature.Error,
                                exceptionHandlerFeature.Error.Message);
                        }

                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An unexpected fault happened. Try again later.");

                    });
                });
            }

             AutoMapper.Mapper.Initialize(cfg =>
             {
                 
                 cfg.CreateMap<ClienteDto, Cliente>().ForMember(x => x.Viaje, opt => opt.Ignore()); ;

                 cfg.CreateMap<TaxistaDto, Taxista>().ForMember(x => x.Viaje, opt => opt.Ignore()); ;

                 cfg.CreateMap<UbicacionDto, Ubicacion>().ForMember(x => x.IdViajeNavigation, opt => opt.Ignore());

                 cfg.CreateMap<ViajeDto, Viaje>().ForMember(x => x.IdClienteNavigation, opt => opt.Ignore())
             .ForMember(x => x.IdTaxistaNavigation, opt => opt.Ignore())
             .ForMember(x => x.Ubicacion, opt => opt.Ignore());


             });
            app.UseMvc();
        }
    }
}
