﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Interfaces;
using API.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Data.Entities;
using Xunit;
using API.Data.Repositories;
using APIUbicacions.Controllers;
using Microsoft.AspNetCore.Http.Extensions;
using APIViajes.Models;

namespace APi.UnitTest
{
    public class UblicacionUnitTest
    {
        UbicacionController _controller;
        IUbicacionRepository _repository;
        IViajeRepository _viajeRepository;
        DemoTaxiContext _context;

        public UblicacionUnitTest()
        {
            _context = new DemoTaxiContext();
            _repository = new UbicacionRepository(_context);
            _viajeRepository = new ViajeRepository(_context);

            _controller = new UbicacionController(_repository, _viajeRepository,null);
            AutoMapper.Mapper.Initialize(cfg =>
            {

                cfg.CreateMap<ClienteDto, Cliente>().ForMember(x => x.Viaje, opt => opt.Ignore());

                cfg.CreateMap<TaxistaDto, Taxista>().ForMember(x => x.Viaje, opt => opt.Ignore());

                cfg.CreateMap<UbicacionDto, Ubicacion>().ForMember(x => x.IdViajeNavigation, opt => opt.Ignore());

                cfg.CreateMap<ViajeDto, Viaje>().ForMember(x => x.IdClienteNavigation, opt => opt.Ignore())
                .ForMember(x => x.IdTaxistaNavigation, opt => opt.Ignore())
                .ForMember(x => x.Ubicacion, opt => opt.Ignore());



            });
        }

        [Fact]
        public void Add_InvalidUbicacion_BadRequest()
        {
            // Arrange
            UbicacionDto Ubicacion = null;

            // Act
            var badResponse = _controller.CreateUbicacion(Ubicacion);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Add_InvalidUbicacionViaje_BadRequest()
        {
            // Arrange
            var Ubicacion = new UbicacionDto()
            {
                IdViaje = new Guid(),
                Latitud = 19.3512987F,
                Longitud = -99.0686532F
            };

            // Act
            var badResponse = _controller.CreateUbicacion(Ubicacion);

            // Assert
            Assert.IsType<NotFoundObjectResult>(badResponse);
        }

        [Fact]
        public void Add_ValidUbicacion_CreatedResponse()
        {
            // Arrange
            var Ubicacion = new UbicacionDto()
            {
                IdViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01"),
                Latitud = 19.3512987F,
                Longitud = -99.0686532F
            };

            // Act
            var createdResponse = _controller.CreateUbicacion(Ubicacion);

            // Assert
            Assert.IsType<CreatedAtRouteResult>(createdResponse);
        }


        [Fact]
        public void Add_ValidUbicacionPassed_CreatedItem()
        {
            // Arrange
            var Ubicacion = new UbicacionDto()
            {
                IdViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01"),
                Latitud = 19.3512987F,
                Longitud = -99.0686532F
            };

            // Act
            var createdResponse = _controller.CreateUbicacion(Ubicacion) as CreatedAtRouteResult;
            var item = createdResponse.Value as UbicacionDto;

            // Assert
            Assert.IsType<UbicacionDto>(item);
            Assert.Equal(new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01"), item.IdViaje);
        }

        [Fact]
        public void Update_InvalidUbicacion_BadRequestUbicacion()
        {
            // Arrange
            UbicacionDto Ubicacion = null;
            Guid idUbicacion = new Guid("2ba6c242-dfa3-4e51-a0ab-b3b893b44e20");
            // Act
            var badResponse = _controller.UpdateUbicacion(Ubicacion, idUbicacion);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Update_InvalidUbicacion_BadRequestGuid()
        {
            // Arrange
             var Ubicacion = new UbicacionDto()
             {
                 IdViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01"),
                 Latitud = 19.3512987F,
                 Longitud = -99.0686532F
             };
            Guid idUbicacion = Guid.Empty;
            // Act
            var badResponse = _controller.UpdateUbicacion(Ubicacion, idUbicacion);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Update_ReturnsNotFoundResult()
        {
            // Act
            var Ubicacion = new UbicacionDto()
            {
                IdViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01"),
                Latitud = 19.3512987F,
                Longitud = -99.0686532F
            };
            Guid idUbicacion = new Guid("2bf6c242-dfa3-4751-a0ab-b3b893b44e20");
            // Arrange

            var notFoundResponse = _controller.UpdateUbicacion(Ubicacion, idUbicacion);

            // Assert
            Assert.IsType<NotFoundObjectResult>(notFoundResponse);
        }


        [Fact]
        public void Update_ValidUbicacion_CreatedResponse()
        {
            // Arrange
            var Ubicacion = new UbicacionDto()
            {
                IdViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01"),
                Latitud = 19.3512987F,
                Longitud = -99.0686532F
            };
            Guid idUbicacion = new Guid("b04f645b-bcad-46bb-8be6-2a870053c83c");
            // Act
            var updatedResponse = _controller.UpdateUbicacion(Ubicacion, idUbicacion);

            // Assert
            Assert.IsType<CreatedAtRouteResult>(updatedResponse);
        }


        [Fact]
        public void Update_ValidUbicacionPassed_CreatedItem()
        {
            // Arrange
            var Ubicacion = new UbicacionDto()
            {
                IdViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01"),
                Latitud = 19.3512987F,
                Longitud = -99.0686532F
            };
            Guid idUbicacion = new Guid("b04f645b-bcad-46bb-8be6-2a870053c83c");
            // Act
            var createdResponse = _controller.UpdateUbicacion(Ubicacion, idUbicacion) as CreatedAtRouteResult;
            var item = createdResponse.Value as UbicacionDto;

            // Assert
            Assert.IsType<UbicacionDto>(item);
            Assert.Equal(new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01"), item.IdViaje);
        }

        [Fact]
        public void Delete_NotExistingUbicacion()
        {
            // Arrange
            var idUbicacion = Guid.NewGuid();

            // Act
            var badResponse = _controller.DeleteUbicacion(idUbicacion);

            // Assert
            Assert.IsType<NotFoundObjectResult>(badResponse);
        }

        [Fact]
        public void Delete_ExistingUbicacion()
        {
            // Arrange
            var idUbicacion = new Guid("2ba6c242-dfa3-4751-a0ab-b3b893b44e20");

            // Act
            var okResponse = _controller.DeleteUbicacion(idUbicacion);

            // Assert
            Assert.IsType<NoContentResult>(okResponse);
        }
        [Fact]
        public void Get_ResulOK()
        {
            UbicacionResourceParameters parameters = new UbicacionResourceParameters();
            // Act
            var okResult = _controller.GetUbicacions(parameters);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }
        [Fact]
        public void Get_Result_ReturnItems()
        {
            // Act
            UbicacionResourceParameters parameters = new UbicacionResourceParameters();
            // Act
            var okResult = _controller.GetUbicacions(parameters);


            // Assert
            var items = Assert.IsAssignableFrom<IEnumerable<UbicacionDto>>(((OkObjectResult)okResult).Value);
            Assert.Equal(1, items.Count());
        }

        [Fact]
        public void GetByIdUbicacion_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.GetUbicacion(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundObjectResult>(notFoundResult);
        }

        [Fact]
        public void GetByIdUbicacion_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("b04f645b-bcad-46bb-8be6-2a870053c83c");

            // Act
            var okResult = _controller.GetUbicacion(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void GetByIdUbicacion_RightItem()
        {
            // Arrange
            var testGuid = new Guid("b04f645b-bcad-46bb-8be6-2a870053c83c");

            // Act
            var okResult = _controller.GetUbicacion(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<UbicacionDto>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as UbicacionDto).IdUbicacion);
        }


    }
}
