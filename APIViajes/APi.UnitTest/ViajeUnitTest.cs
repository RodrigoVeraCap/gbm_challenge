﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Interfaces;
using API.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Data.Entities;
using APIViajes.Models;
using Xunit;
using API.Data.Repositories;
using APIViajes.Controllers;
using Microsoft.AspNetCore.Http.Extensions;

namespace APi.UnitTest
{
    public class ViajeUnitTest
    {
        ViajeController _controller;
        IViajeRepository _repository;
        IClienteRepository _clienteRepository;
        ITaxistaRepository _taxistaRepository;
        DemoTaxiContext _context;

        public ViajeUnitTest()
        {
            _context = new DemoTaxiContext();
            _repository = new ViajeRepository(_context);
            _clienteRepository = new ClienteRepository(_context);
            _taxistaRepository = new TaxistaRepository(_context);
            _controller = new ViajeController(_repository,_clienteRepository,_taxistaRepository,null);
            AutoMapper.Mapper.Initialize(cfg =>
            {

                cfg.CreateMap<ClienteDto, Cliente>().ForMember(x => x.Viaje, opt => opt.Ignore()); 

                cfg.CreateMap<TaxistaDto, Taxista>().ForMember(x => x.Viaje, opt => opt.Ignore());

                cfg.CreateMap<Ubicacion, UbicacionDto>();

                cfg.CreateMap<ViajeDto, Viaje>().ForMember(x => x.IdClienteNavigation, opt => opt.Ignore())
                .ForMember(x => x.IdTaxistaNavigation, opt => opt.Ignore())
                .ForMember(x => x.Ubicacion, opt => opt.Ignore());



            });
        }

        [Fact]
        public void Add_InvalidViaje_BadRequest()
        {
            // Arrange
            ViajeDto Viaje = null;

            // Act
            var badResponse = _controller.CreateViaje(Viaje);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Add_InvalidViajeCliente_BadRequest()
        {
            // Arrange
            var Viaje = new ViajeDto()
            {
                IdCliente = new Guid(),
                IdTaxista = new Guid("2bb34ceb-c164-4935-8b4a-0b6fc5c3e879"),
                ViajeInicioLatitud = 19.3512987F,
                ViajeInicioLongitud = -99.0686532F,
                ViajeFinalLongitud = 15.3512987F,
                ViajeFinalLatitud = -98.0686532F,
            };

            // Act
            var badResponse = _controller.CreateViaje(Viaje);

            // Assert
            Assert.IsType<NotFoundObjectResult>(badResponse);
        }

        [Fact]
        public void Add_InvalidViajeTaxista_BadRequest()
        {
            // Arrange
            var Viaje = new ViajeDto()
            {
                IdCliente = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"),
                IdTaxista = new Guid(),
                ViajeInicioLatitud = 19.3512987F,
                ViajeInicioLongitud = -99.0686532F,
                ViajeFinalLongitud = 15.3512987F,
                ViajeFinalLatitud = -98.0686532F,
            };

            // Act
            var badResponse = _controller.CreateViaje(Viaje);

            // Assert
            Assert.IsType<NotFoundObjectResult>(badResponse);
        }


        [Fact]
        public void Add_ValidViaje_CreatedResponse()
        {
            // Arrange
            var Viaje = new ViajeDto()
            {
                IdCliente = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"),
                IdTaxista = new Guid("2bb34ceb-c164-4935-8b4a-0b6fc5c3e879"),
                ViajeInicioLatitud = 19.3512987F,
                ViajeInicioLongitud = -99.0686532F,
                ViajeFinalLongitud = 15.3512987F,
                ViajeFinalLatitud = -98.0686532F,
            };

            // Act
            var createdResponse = _controller.CreateViaje(Viaje);

            // Assert
            Assert.IsType<CreatedAtRouteResult>(createdResponse);
        }


        [Fact]
        public void Add_ValidViajePassed_CreatedItem()
        {
            // Arrange
            var Viaje = new ViajeDto()
            {
                IdCliente = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"),
                IdTaxista = new Guid("2bb34ceb-c164-4935-8b4a-0b6fc5c3e879"),
                ViajeInicioLatitud = 11.3512987F,
                ViajeInicioLongitud = -89.0686532F,
                ViajeFinalLongitud = 13.3512987F,
                ViajeFinalLatitud = -88.0686532F,
            };

            // Act
            var createdResponse = _controller.CreateViaje(Viaje) as CreatedAtRouteResult;
            var item = createdResponse.Value as ViajeDto;

            // Assert
            Assert.IsType<ViajeDto>(item);
            Assert.Equal(new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"), item.IdCliente);
        }

        [Fact]
        public void Update_InvalidViaje_BadRequestViaje()
        {
            // Arrange
            ViajeDto Viaje = null;
            Guid idViaje = new Guid("5c7e5671-5cf3-491f-a538-3564cf9804cc");
            // Act
            var badResponse = _controller.UpdateViaje(Viaje, idViaje);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Update_InvalidViaje_BadRequestGuid()
        {
            // Arrange
            var Viaje = new ViajeDto()
            {
                IdCliente = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"),
                IdTaxista = new Guid("2bb34ceb-c164-4935-8b4a-0b6fc5c3e879"),
                ViajeInicioLatitud = 11.3512987F,
                ViajeInicioLongitud = -89.0686532F,
                ViajeFinalLongitud = 13.3512987F,
                ViajeFinalLatitud = -88.0686532F,
            };
            Guid idViaje = Guid.Empty;
            // Act
            var badResponse = _controller.UpdateViaje(Viaje, idViaje);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Update_ReturnsNotFoundResult()
        {
            // Act
            var Viaje = new ViajeDto()
            {
                IdCliente = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"),
                IdTaxista = new Guid("2bb34ceb-c164-4935-8b4a-0b6fc5c3e879"),
                ViajeInicioLatitud = 11.3512987F,
                ViajeInicioLongitud = -89.0686532F,
                ViajeFinalLongitud = 13.3512987F,
                ViajeFinalLatitud = -88.0686532F,
            };
            Guid idViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01");
            // Arrange

            var notFoundResponse = _controller.UpdateViaje(Viaje, idViaje);

            // Assert
            Assert.IsType<NotFoundObjectResult>(notFoundResponse);
        }


        [Fact]
        public void Update_ValidViaje_CreatedResponse()
        {
            // Arrange
            var Viaje = new ViajeDto()
            {
                IdCliente = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"),
                IdTaxista = new Guid("2bb34ceb-c164-4935-8b4a-0b6fc5c3e879"),
                ViajeInicioLatitud = 11.3512987F,
                ViajeInicioLongitud = -89.0686532F,
                ViajeFinalLongitud = 13.3512987F,
                ViajeFinalLatitud = -88.0686532F,
            };
            Guid idViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01");
            // Act
            var updatedResponse = _controller.UpdateViaje(Viaje, idViaje);

            // Assert
            Assert.IsType<CreatedAtRouteResult>(updatedResponse);
        }


        [Fact]
        public void Update_ValidViajePassed_CreatedItem()
        {
            // Arrange
            var Viaje = new ViajeDto()
            {
                IdCliente = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"),
                IdTaxista = new Guid("2bb34ceb-c164-4935-8b4a-0b6fc5c3e879"),
                ViajeInicioLatitud = 11.3512987F,
                ViajeInicioLongitud = -89.0686532F,
                ViajeFinalLongitud = 13.3512987F,
                ViajeFinalLatitud = -88.0686532F,
            };
            Guid idViaje = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01");
            // Act
            var createdResponse = _controller.UpdateViaje(Viaje, idViaje) as CreatedAtRouteResult;
            var item = createdResponse.Value as ViajeDto;

            // Assert
            Assert.IsType<ViajeDto>(item);
            Assert.Equal(new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd"), item.IdCliente);
        }

        [Fact]
        public void Delete_NotExistingViaje()
        {
            // Arrange
            var idViaje = Guid.NewGuid();

            // Act
            var badResponse = _controller.DeleteViaje(idViaje);

            // Assert
            Assert.IsType<NotFoundObjectResult>(badResponse);
        }

        [Fact]
        public void Delete_ExistingViaje()
        {
            // Arrange
            var idViaje = new Guid("e0818da2-087e-470e-8a91-d6b56e698797");

            // Act
            var okResponse = _controller.DeleteViaje(idViaje);

            // Assert
            Assert.IsType<NoContentResult>(okResponse);
        }
        [Fact]
        public void Get_ResulOK()
        {
            ViajeResourceParameters parameters = new ViajeResourceParameters();
            // Act
            var okResult = _controller.GetViajes(parameters);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }
        [Fact]
        public void Get_Result_ReturnItems()
        {
            // Act
            ViajeResourceParameters parameters = new ViajeResourceParameters();
            // Act
            var okResult = _controller.GetViajes(parameters);


            // Assert
            var items = Assert.IsAssignableFrom<IEnumerable<ViajeDto>>(((OkObjectResult)okResult).Value);
            Assert.Equal(1, items.Count());
        }

        [Fact]
        public void GetByIdViaje_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.GetViaje(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void GetByIdViaje_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01");

            // Act
            var okResult = _controller.GetViaje(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void GetByIdViaje_RightItem()
        {
            // Arrange
            var testGuid = new Guid("9d75d7ab-e59f-4d39-8033-00395368ee01");

            // Act
            var okResult = _controller.GetViaje(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<ViajeDto>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as ViajeDto).IdViaje);
        }


    }
}
