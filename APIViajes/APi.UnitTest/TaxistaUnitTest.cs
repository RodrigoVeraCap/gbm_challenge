using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Interfaces;
using API.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Data.Entities;
using APIViajes.Models;
using Xunit;
using API.Data.Repositories;
using APIViajes.Controllers;
using Microsoft.AspNetCore.Http.Extensions;

namespace APi.UnitTest
{
    public class TaxistaUnitTest
    {
        TaxistaController _controller;
        ITaxistaRepository _repository;
        DemoTaxiContext _context;

        public TaxistaUnitTest()
        {
            _context = new DemoTaxiContext();
            _repository = new TaxistaRepository(_context); 
            _controller = new TaxistaController(_repository,null);
            AutoMapper.Mapper.Initialize(cfg =>
            {

                cfg.CreateMap<ClienteDto, Cliente>().ForMember(x => x.Viaje, opt => opt.Ignore()); ;

                cfg.CreateMap<TaxistaDto, Taxista>().ForMember(x => x.Viaje, opt => opt.Ignore()); ;

                cfg.CreateMap<Ubicacion, UbicacionDto>();

                cfg.CreateMap<Viaje, ViajeDto>();


            });
        }

        [Fact]
        public void Add_InvalidTaxista_BadRequest()
        {
            // Arrange
            TaxistaDto taxista = null;
          
            // Act
            var badResponse = _controller.CreateTaxista(taxista);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }


        [Fact]
        public void Add_ValidTaxista_CreatedResponse()
        {
            // Arrange
            var taxista = new TaxistaDto()
            {
                Nombre = "Saul",
                ApellidoPaterno = "Hernandez",
                ApellidoMaterno = "Lopez",
                Placas = "223-DFG"
            };

            // Act
            var createdResponse = _controller.CreateTaxista(taxista);

            // Assert
            Assert.IsType<CreatedAtRouteResult>(createdResponse);
        }


        [Fact]
        public void Add_ValidTaxistaPassed_CreatedItem()
        {
            // Arrange
            var taxista = new TaxistaDto()
            {
                Nombre = "Saul",
                ApellidoPaterno = "Hernandez",
                ApellidoMaterno = "Lopez",
                Placas = "223-DFG"
            };

            // Act
            var createdResponse = _controller.CreateTaxista(taxista) as CreatedAtRouteResult;
            var item = createdResponse.Value as TaxistaDto;

            // Assert
            Assert.IsType<TaxistaDto>(item);
            Assert.Equal("Saul", item.Nombre);
        }

        [Fact]
        public void Update_InvalidTaxista_BadRequestTaxista()
        {
            // Arrange
            TaxistaDto taxista = null;
            Guid idTaxista = new Guid("5c7e5671-5cf3-491f-a538-3564cf9804cc");
            // Act
            var badResponse = _controller.UpdateTaxista(taxista,idTaxista);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Update_InvalidTaxista_BadRequestGuid()
        {
            // Arrange
            var taxista = new TaxistaDto()
            {
                Nombre = "Saul",
                ApellidoPaterno = "Hernandez",
                ApellidoMaterno = "Lopez",
                Placas = "223-DFG"
            };
            Guid idTaxista = Guid.Empty;
            // Act
            var badResponse = _controller.UpdateTaxista(taxista,idTaxista);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Update_ReturnsNotFoundResult()
        {
            // Act
            var taxista = new TaxistaDto()
            {
                Nombre = "Saul",
                ApellidoPaterno = "Hernandez",
                ApellidoMaterno = "Lopez",
                Placas = "223-DFG"
            };
            Guid idTaxista = new Guid("5c7e5111-5cf3-491f-a538-3564cf9804cc");
            // Arrange

            var notFoundResponse = _controller.UpdateTaxista(taxista, idTaxista);

            // Assert
               Assert.IsType<NotFoundObjectResult>(notFoundResponse);
        }


        [Fact]
        public void Update_ValidTaxista_CreatedResponse()
        {
            // Arrange
            var taxista = new TaxistaDto()
            {
                Nombre = "Raul",
                ApellidoPaterno = "Hernandez",
                ApellidoMaterno = "Lopez",
                Placas = "223-DFG"
            };
            Guid idTaxista = new Guid("5c7e5671-5cf3-491f-a538-3564cf9804cc");
            // Act
            var updatedResponse = _controller.UpdateTaxista (taxista,idTaxista);

            // Assert
            Assert.IsType<CreatedAtRouteResult>(updatedResponse);
        }


        [Fact]
        public void Update_ValidTaxistaPassed_CreatedItem()
        {
            // Arrange
            var taxista = new TaxistaDto()
            {
                Nombre = "Ernesto",
                ApellidoPaterno = "Hernandez",
                ApellidoMaterno = "Lopez",
                Placas = "223-DFG"
            };
            Guid idTaxista = new Guid("5c7e5671-5cf3-491f-a538-3564cf9804cc");
            // Act
            var createdResponse = _controller.UpdateTaxista(taxista,idTaxista) as CreatedAtRouteResult;
            var item = createdResponse.Value as TaxistaDto;

            // Assert
            Assert.IsType<TaxistaDto>(item);
            Assert.Equal("Ernesto", item.Nombre);
        }

        [Fact]
        public void Delete_NotExistingTaxista()
        {
            // Arrange
            var idTaxista = Guid.NewGuid();

            // Act
            var badResponse = _controller.DeleteTaxista(idTaxista);

            // Assert
            Assert.IsType<NotFoundObjectResult>(badResponse);
        }

        [Fact]
        public void Delete_ExistingTaxista()
        {
            // Arrange
            var idTaxista = new Guid("f1e5b97d-e043-4bf2-8c1f-34519b526680");

            // Act
            var okResponse = _controller.DeleteTaxista(idTaxista);

            // Assert
            Assert.IsType<NoContentResult>(okResponse);
        }
        [Fact]
        public void Get_ResulOK()
        {
            TaxistaResourceParameters parameters = new TaxistaResourceParameters();
            // Act
            var okResult = _controller.GetTaxistas(parameters);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }
        [Fact]
        public void Get_Result_ReturnItems()
        {
            // Act
            TaxistaResourceParameters parameters = new TaxistaResourceParameters();
            // Act
            var okResult = _controller.GetTaxistas(parameters);

       
            // Assert
            var items = Assert.IsAssignableFrom<IEnumerable<TaxistaDto>>(((OkObjectResult)okResult).Value);
            Assert.Equal(10, items.Count());
        }

        [Fact]
        public void GetByIdTaxista_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.GetTaxista(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundObjectResult>(notFoundResult);
        }

        [Fact]
        public void GetByIdTaxista_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("6645bc1f-3b42-4f99-b280-a437046cc3f0");

            // Act
            var okResult = _controller.GetTaxista(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void GetByIdTaxista_RightItem()
        {
            // Arrange
            var testGuid = new Guid("6645bc1f-3b42-4f99-b280-a437046cc3f0");

            // Act
            var okResult = _controller.GetTaxista(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<TaxistaDto>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as TaxistaDto).IdTaxista);
        }


    }
}
