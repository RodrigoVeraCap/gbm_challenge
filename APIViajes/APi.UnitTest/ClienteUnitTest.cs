﻿using API.Data.Entities;
using API.Data.Interfaces;
using API.Data.Repositories;
using API.Helpers;
using APIViajes.Controllers;
using APIViajes.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace APi.UnitTest
{
    public class ClienteUnitTest
    {
        ClienteController _controller;
        IClienteRepository _repository;
        DemoTaxiContext _context;

        public ClienteUnitTest()
        {
            _context = new DemoTaxiContext();
            _repository = new ClienteRepository(_context);
            _controller = new ClienteController(_repository, null);
            AutoMapper.Mapper.Initialize(cfg =>
            {

                cfg.CreateMap<TaxistaDto, Taxista>().ForMember(x => x.Viaje, opt => opt.Ignore()); ;

                cfg.CreateMap<ClienteDto, Cliente>().ForMember(x => x.Viaje, opt => opt.Ignore()); ;

                cfg.CreateMap<Ubicacion, UbicacionDto>();

                cfg.CreateMap<Viaje, ViajeDto>();


            });
        }

        [Fact]
        public void Add_InvalidCliente_BadRequest()
        {
            // Arrange
            ClienteDto Cliente = null;

            // Act
            var badResponse = _controller.CreateCliente(Cliente);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }


        [Fact]
        public void Add_ValidCliente_CreatedResponse()
        {
            // Arrange
            var Cliente = new ClienteDto()
            {
                Nombre = "Pedro",
                ApellidoPaterno = "Ramirez",
                ApellidoMaterno = "Zarate",
            };

            // Act
            var createdResponse = _controller.CreateCliente(Cliente);

            // Assert
            Assert.IsType<CreatedAtRouteResult>(createdResponse);
        }


        [Fact]
        public void Add_ValidClientePassed_CreatedItem()
        {
            // Arrange
            var Cliente = new ClienteDto()
            {
                Nombre = "Roberto",
                ApellidoPaterno = "Jimenez",
                ApellidoMaterno = "Sanchez",
            };

            // Act
            var createdResponse = _controller.CreateCliente(Cliente) as CreatedAtRouteResult;
            var item = createdResponse.Value as ClienteDto;

            // Assert
            Assert.IsType<ClienteDto>(item);
            Assert.Equal("Roberto", item.Nombre);
        }

        [Fact]
        public void Update_InvalidCliente_BadRequestCliente()
        {
            // Arrange
            ClienteDto Cliente = null;
            Guid idCliente = new Guid("5c7e5671-5cf3-491f-a538-3564cf9804cc");
            // Act
            var badResponse = _controller.UpdateCliente(Cliente, idCliente);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Update_InvalidCliente_BadRequestGuid()
        {
            // Arrange
            var Cliente = new ClienteDto()
            {
                Nombre = "Pedro",
                ApellidoPaterno = "Centeno",
                ApellidoMaterno = "Lopez",
            };
            Guid idCliente = Guid.Empty;
            // Act
            var badResponse = _controller.UpdateCliente(Cliente, idCliente);

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void Update_ReturnsNotFoundResult()
        {
            // Act
            var Cliente = new ClienteDto()
            {
                Nombre = "Pedro",
                ApellidoPaterno = "Centeno",
                ApellidoMaterno = "Lopez",
            };
            Guid idCliente = new Guid("5c7e5111-5cf3-491f-a538-3564cf9804cc");
            // Arrange

            var notFoundResponse = _controller.UpdateCliente(Cliente, idCliente);

            // Assert
            Assert.IsType<NotFoundObjectResult>(notFoundResponse);
        }


        [Fact]
        public void Update_ValidCliente_CreatedResponse()
        {
            // Arrange
            var Cliente = new ClienteDto()
            {
                Nombre = "Pedro",
                ApellidoPaterno = "Centeno",
                ApellidoMaterno = "Juarez",
            };
            Guid idCliente = new Guid("f9d09c46-b41c-44ed-b399-a8b2ac1cf1b3");
            // Act
            var updatedResponse = _controller.UpdateCliente(Cliente, idCliente);

            // Assert
            Assert.IsType<CreatedAtRouteResult>(updatedResponse);
        }


        [Fact]
        public void Update_ValidClientePassed_CreatedItem()
        {
            // Arrange
            var Cliente = new ClienteDto()
            {
                Nombre = "Ernesto",
                ApellidoPaterno = "Cabrera",
                ApellidoMaterno = "Ramirez",
            };
            Guid idCliente = new Guid("f9d09c46-b41c-44ed-b399-a8b2ac1cf1b3");
            // Act
            var createdResponse = _controller.UpdateCliente(Cliente,idCliente) as CreatedAtRouteResult;
            var item = createdResponse.Value as ClienteDto;

            // Assert
            Assert.IsType<ClienteDto>(item);
            Assert.Equal("Ernesto", item.Nombre);
        }

        [Fact]
        public void Delete_NotExistingCliente()
        {
            // Arrange
            var idCliente = Guid.NewGuid();

            // Act
            var badResponse = _controller.DeleteCliente(idCliente);

            // Assert
            Assert.IsType<NotFoundObjectResult>(badResponse);
        }

        [Fact]
        public void Delete_ExistingCliente()
        {
            // Arrange
            var idCliente = new Guid("47801b4e-4322-4ee7-9750-f480be8abdff");

            // Act
            var okResponse = _controller.DeleteCliente(idCliente);

            // Assert
            Assert.IsType<NoContentResult>(okResponse);
        }
        [Fact]
        public void Get_ResulOK()
        {
            ClienteResourceParameters parameters = new ClienteResourceParameters();
            // Act
            var okResult = _controller.GetClientes(parameters);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }
        [Fact]
        public void Get_Result_ReturnItems()
        {
            // Act
            ClienteResourceParameters parameters = new ClienteResourceParameters();
            // Act
            var okResult = _controller.GetClientes(parameters);


            // Assert
            var items = Assert.IsAssignableFrom<IEnumerable<ClienteDto>>(((OkObjectResult)okResult).Value);
            Assert.Equal(2, items.Count());
        }

        [Fact]
        public void GetByIdCliente_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.GetCliente(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundObjectResult>(notFoundResult);
        }

        [Fact]
        public void GetByIdCliente_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd");

            // Act
            var okResult = _controller.GetCliente(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void GetByIdCliente_RightItem()
        {
            // Arrange
            var testGuid = new Guid("c2112154-d356-4eea-adcc-a1f95cabe1bd");

            // Act
            var okResult = _controller.GetCliente(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<ClienteDto>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as ClienteDto).IdCliente);
        }


    }
}
